﻿using Elika.Native;
using SkiaSharp;
using System.Drawing;

namespace Elika
{
    public class FrameBufferRenderer : IDisposable
    {
        private SKBitmap? _bitmap;

		internal FrameBufferRenderer(FrameBufferReference reference)
		{
			FrameBufferRef = reference;
		}

		public Size ScreenSize => FrameBufferRef.ScreenSize;

        public FrameBufferReference FrameBufferRef { get; private set; }

        public bool IsDisposed { get; private set; }


        public void Render(DelRenderCallback callback)
		{
			ArgumentNullException.ThrowIfNull(callback);


            var resolution = ScreenSize;

			var width = resolution.Width;
			var height = resolution.Height;

			var info = new SKImageInfo(width, height, SKImageInfo.PlatformColorType, SKAlphaType.Premul);

			var isForce = false;

			if (_bitmap == null || info.Width != _bitmap.Width || info.Height != _bitmap.Height)
			{
				_bitmap = new SKBitmap(width, height, FrameBufferRef.PixelFormat, SKAlphaType.Premul);
				isForce = true;
			}

            using var surface = SKSurface.Create(info, _bitmap.GetPixels(out _));

            callback.Invoke(new Size(width, height), surface.Canvas, isForce);

			Render(_bitmap);
        }

        public void Render(SKBitmap bitmap)
        {
            Render(FrameBufferRef, bitmap);
        }

        internal static void Render(FrameBufferReference reference, SKBitmap bitmap)
        {
			ArgumentNullException.ThrowIfNull(reference);
			ArgumentNullException.ThrowIfNull(bitmap);

            reference.VSync();

            var pixels = bitmap.GetPixels(out _);
            var isOutOfBound = reference.RowBytes != bitmap.BytesPerPixel * bitmap.Width;
            if (isOutOfBound)
            {
                var bitmapRowBytes = bitmap.RowBytes;
                var bitmapBytesPerPixel = bitmap.BytesPerPixel;

                var len = new IntPtr(bitmap.Width * bitmapBytesPerPixel);
                for (var line = 0; line < bitmap.Height; line++)
                {
                    var dstAddr = reference.BufferAddress + line * reference.RowBytes;
                    var srcAddr = pixels + line * bitmapRowBytes;
                    
                    Libc.memcpy(
                        reference.BufferAddress + line * reference.RowBytes,
                        pixels + line * bitmapRowBytes,
                        len);
                }
            }
            else
            {
                var len = new IntPtr(reference.RowBytes * bitmap.Height);
                Libc.memcpy(reference.BufferAddress, pixels, len);
            }
        }

        public static FrameBufferRenderer Create(string fileName)
        {
            var reference = FrameBufferReference.Create(fileName);
            var renderer = new FrameBufferRenderer(reference);

            return renderer;
        }

        public void Dispose()
        {
            FrameBufferRef.Dispose();
            _bitmap?.Dispose();
            GC.SuppressFinalize(this);
            IsDisposed = true;
        }
    }
}
