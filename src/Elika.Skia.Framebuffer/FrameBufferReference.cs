﻿using Elika.Native;
using SkiaSharp;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Elika
{
    public unsafe class FrameBufferReference : IDisposable
    {
        private int _fd;
		private fb_fix_screeninfo _fixedInfo;
		private fb_var_screeninfo _screenInfo;
		private IntPtr _mappedLength;
		private IntPtr _mappedAddress;


		public FrameBufferReference(int fd)
        {
            _fd = fd;
        }

		/// <summary>
		/// Size of the screen in pixels
		/// </summary>
		public Size ScreenSize { get; private set; }

		/// <summary>
		/// Number of bytes for an individual row
		/// </summary>
		public int RowBytes => (int)_fixedInfo.line_length;

        /// <summary>
        /// Pixel format information
        /// </summary>
        public SKColorType PixelFormat
            => _screenInfo.bits_per_pixel == 16 ? SKColorType.Rgb565 : _screenInfo.blue.offset == 16 ? SKColorType.Rgba8888 : SKColorType.Bgra8888;

        public bool IsDisposed { get; private set; }

        private void Set32BitsPixelFormat()
		{
			_screenInfo.bits_per_pixel = 32;
			_screenInfo.grayscale = 0;
			_screenInfo.red = new fb_bitfield { length = 8 };
			_screenInfo.blue = new fb_bitfield { length = 8 };
			_screenInfo.green = new fb_bitfield { length = 8 };
			_screenInfo.transp = new fb_bitfield { length = 8 };
			_screenInfo.green.offset = 8;
			_screenInfo.blue.offset = 16;
			_screenInfo.transp.offset = 24;
		}

		private void ThrowIfDisposed()
        {
			if (_fd <= 0)
				throw new ObjectDisposedException(nameof(FrameBufferReference));
		}

		public void Init()
        {
			fixed (void* pScreenInfo = &_screenInfo)
			{
				if (Libc.ioctl(_fd, FbIoCtl.FBIOGET_VSCREENINFO, pScreenInfo) == -1)
				{
					throw new InvalidOperationException($"Failed to invoke FBIOGET_VSCREENINFO ({Marshal.GetLastWin32Error()}");
				}

				Set32BitsPixelFormat();

				if (Libc.ioctl(_fd, FbIoCtl.FBIOPUT_VSCREENINFO, pScreenInfo) == -1)
				{
					_screenInfo.transp = new fb_bitfield();
				}

				Libc.ioctl(_fd, FbIoCtl.FBIOPUT_VSCREENINFO, pScreenInfo);

				if (Libc.ioctl(_fd, FbIoCtl.FBIOGET_VSCREENINFO, pScreenInfo) == -1)
				{
					throw new InvalidOperationException($"Failed to invoke FBIOGET_VSCREENINFO ({Marshal.GetLastWin32Error()}");
				}

				if (_screenInfo.bits_per_pixel != 32)
				{
					throw new InvalidOperationException("Failed to set 32 bits display mode");
				}
			}

			ScreenSize = new Size((int)_screenInfo.xres, (int)_screenInfo.yres);

			fixed (void* pFixedInfo = &_fixedInfo)
			{
				if (Libc.ioctl(_fd, FbIoCtl.FBIOGET_FSCREENINFO, pFixedInfo) == -1)
				{
					throw new InvalidOperationException($"Failed to invoke FBIOGET_FSCREENINFO ({Marshal.GetLastWin32Error()})");
				}
			}

			_mappedLength = new IntPtr(_fixedInfo.line_length * _screenInfo.yres);
			_mappedAddress = Libc.mmap(
				IntPtr.Zero,
				_mappedLength,
				Libc.PROT_READ | Libc.PROT_WRITE,
				Libc.MAP_SHARED,
				_fd,
				IntPtr.Zero);

			if (_mappedAddress == new IntPtr(-1))
			{
				throw new InvalidOperationException($"Failed to map {_mappedLength} bytes ({Marshal.GetLastWin32Error()})");
			}
		}

		public IntPtr BufferAddress
		{
			get
			{
				ThrowIfDisposed();

				return _mappedAddress;
			}
		}

		private void Dispose(bool disposing)
		{
			if (_mappedAddress != IntPtr.Zero)
			{
				Libc.munmap(_mappedAddress, _mappedLength);
				_mappedAddress = IntPtr.Zero;
			}

			if (_fd == 0)
				return;

			Libc.close(_fd);
			_fd = 0;
		}

		public void VSync()
		{
			ThrowIfDisposed();

			Libc.ioctl(_fd, FbIoCtl.FBIO_WAITFORVSYNC, null);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
            IsDisposed = true;

        }

		internal static FrameBufferReference Create(string fileName)
        {
			var fd = Libc.open(fileName, Libc.O_RDWR, 0);
			var fb = new FrameBufferReference(fd);
			fb.Init();
			return fb;
		}
	}
}
