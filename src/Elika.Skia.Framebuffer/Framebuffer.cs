﻿using System.Collections.Concurrent;

namespace Elika
{
    public class FrameBuffer
    {
        private static ConcurrentDictionary<string, FrameBufferRenderer> _contexts = new();

        public static FrameBufferRenderer GetOrOpen()
        {
            var fileName =
                Environment.GetEnvironmentVariable("FRAMEBUFFER")
                ?? "/dev/fb0";

            return GetOrOpen(fileName);
        }

        public static FrameBufferRenderer GetOrOpen(int number)
        {
            if (number < 0)
                throw new ArgumentException("", nameof(number));

            return GetOrOpen($"/dev/fb{number}");
        }

        public static FrameBufferRenderer GetOrOpen(string fileName)
        {
            return _contexts.GetOrAdd(fileName, FrameBufferRenderer.Create);
        }
    }
}