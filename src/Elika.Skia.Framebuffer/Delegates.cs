﻿using SkiaSharp;
using System.Drawing;

namespace Elika
{
    public delegate void DelRenderCallback(Size screen, SKCanvas canvas, bool forceRedraw);
}
