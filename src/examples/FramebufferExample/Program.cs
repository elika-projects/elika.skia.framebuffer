﻿// See https://aka.ms/new-console-template for more information

using Elika;
using SkiaSharp;
using System.Diagnostics;

if (args.Contains("--wait-for-attach"))
{
    Console.WriteLine("Attach debugger and use 'Set next statement'");
    while (true)
    {
        Thread.Sleep(100);
        if (Debugger.IsAttached)
            break;
    }
}

var renderer = FrameBuffer.GetOrOpen();
renderer.Render((screen, canvas, _) =>
{
    canvas.Clear(SKColors.White);
    canvas.DrawRect(30, 30, screen.Width - 60, screen.Height - 60, new SKPaint() { Color = SKColors.Brown });
});

var isPressed = false;

var th = new Thread(() =>
{
    Console.CursorVisible = false;
    while (Console.ReadKey(true).Key is not ConsoleKey.Escape)
    {
    }

    isPressed = true;
})
{
    IsBackground = true
};
th.Start();

var spin = new SpinWait();
while (!isPressed)
    spin.SpinOnce();


